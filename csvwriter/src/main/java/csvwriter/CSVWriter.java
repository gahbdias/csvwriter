package csvwriter;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.util.Scanner;

public class CSVWriter {
    public static final int maxCoordinate = 1000000;

    public void run(){
        System.out.println("Enter the number of lines: ");
        Scanner input = new Scanner(System.in);
        int lines = input.nextInt();
        input.close();

        try (PrintWriter writer = new PrintWriter("test.csv")) {
            StringBuilder header = new StringBuilder();
            header.append("value");
            header.append(',');
            header.append("x");
            header.append(',');
            header.append("y");
            header.append('\n');
            writer.write(header.toString());

            SecureRandom random = new SecureRandom();
            int valueInt;
            int valueDecimal;

            int x;
            int y;

            for(double i = 1; i <= lines; i++){
                StringBuilder line = new StringBuilder();
                valueInt = random.nextInt(38) * (random.nextBoolean() ? -1 : 1);
                valueDecimal = random.nextInt(99999999);
                line.append(String.valueOf(valueInt) + "." + String.valueOf(valueDecimal));
                line.append(',');

                x = random.nextInt(maxCoordinate) * (random.nextBoolean() ? -1 : 1);
                line.append(String.valueOf(x));
                line.append(',');

                y = random.nextInt(maxCoordinate) * (random.nextBoolean() ? -1 : 1);
                line.append(String.valueOf(y));
                line.append('\n');
                
                writer.write(line.toString());
            }
      
            System.out.println("CSV generated!");
          } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }    
    }    
}
